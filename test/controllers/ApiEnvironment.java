package controllers;

import play.Environment;

public class ApiEnvironment extends Environment {

    public ApiEnvironment(play.api.Environment environment) {
        super(environment);
    }
}
