package controllers;

import akka.stream.Materializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import play.Application;
import play.test.Helpers;

/**
 * Provides an application for JUnit tests. Make your test class extend this class and an
 * application will be started before each test is invoked. You can setup the application to use by
 * overriding the provideApplication method. Within a test, the running application is available
 * through the app field.
 */
public abstract class WithApplicationJUnit5 {

    protected Application app;

    protected Materializer mat;

    protected Application provideApplication() {
        return Helpers.fakeApplication();
    }

    protected <T> T instanceOf(Class<T> clazz) {
        return app.injector().instanceOf(clazz);
    }

    @BeforeEach
    void startPlay() {
        app = provideApplication();
        Helpers.start(app);
        mat = app.asScala().materializer();
    }

    @AfterEach
    void stopPlay() {
        if (app != null) {
            Helpers.stop(app);
            app = null;
        }
    }
}

