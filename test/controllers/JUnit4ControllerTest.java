package controllers;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

public class JUnit4ControllerTest {

    public static class テスト {
        @Test
        public void ネストされたJUNIT4テスト() {
            assertEquals(2, 1 + 1);
        }
    }
}
