package controllers;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.Helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@NoArgsConstructor
public class TestServerExtension
        implements BeforeAllCallback, AfterAllCallback{

    public Application app;

    private List<AbstractModule> overrideModuleList;

    // バインディングを処理するためのラムダ式
    private Consumer<Binder> binderConsumer;
    /**
     * モジュール追加用テストサーバーのコンストラクタ
     * @param overrideModules 追加したいAbstractModuleの配列
     */
    public TestServerExtension(AbstractModule... overrideModules) {
        this.overrideModuleList = Arrays.asList(overrideModules);
    }

    /**
     * ラムダ式でバインディングを行うためのコンストラクタ
     * @param binderConsumer Binderを引数とするConsumer
     */
    public TestServerExtension(Consumer<Binder> binderConsumer) {
        this.binderConsumer = binderConsumer;
    }


    /**
     * 全てのテストが始まる前に一度だけ呼ばれるメソッド
     * @param context ExtensionContext
     */
    @Override
    public void beforeAll(ExtensionContext context) {
        GuiceApplicationBuilder appBuilder = new GuiceApplicationBuilder();

        // 追加モジュールの適用
        if (overrideModuleList != null) {
            appBuilder = appBuilder.overrides(overrideModuleList.toArray(new AbstractModule[] {}));
        }

        // ラムダ式でのバインディング適用
        if (binderConsumer != null) {
            appBuilder = appBuilder.overrides(new AbstractModule() {
                @Override
                protected void configure() {
                    binderConsumer.accept(binder());
                }
            });
        }
        app = appBuilder.build();
        Helpers.start(app);

    }

    /**
     * 全てのテストが終わった後に一度だけ呼ばれるメソッド
     * @param context ExtensionContext
     */
    @Override
    public void afterAll(ExtensionContext context) {
        if (app != null) {
            Helpers.stop(app);
        }
        overrideModuleList = null;
    }

}