name := """play-juni5-examples"""
organization := "com.haryoiro"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.11"

libraryDependencies += guice

libraryDependencies ++= Seq (
  "org.junit.jupiter" % "junit-jupiter" % "5.9.3" % Test,
  "org.junit.jupiter" % "junit-jupiter-api" % "5.9.3" % Test,
  "org.junit.jupiter" % "junit-jupiter-engine" % "5.9.3" % Test,
  "org.junit.vintage" % "junit-vintage-engine" % "5.9.3" % Test,
  "org.assertj" % "assertj-core" % "3.24.2" % Test,
  "net.aichler" % "jupiter-interface" % "0.11.1" % Test,
  "org.projectlombok" % "lombok" % "1.18.28" % "provided"
)