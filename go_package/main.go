package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func main() {
	filePattern := "./**/test-reports/*.xml"

	filePaths, err := filepath.Glob(filePattern)
	if err != nil {
		fmt.Printf("エラー: ファイルパターンの取得に失敗しました：%v\n", err)
		return
	}

	if len(filePaths) == 0 {
		fmt.Println("対象のファイルが見つかりませんでした")
		return
	}

	for _, filePath := range filePaths {
		err := compressFile(filePath)
		if err != nil {
			fmt.Printf("エラー: ファイルの圧縮に失敗しました：%v\n", err)
		} else {
			fmt.Printf("ファイルを圧縮しました: %s\n", filePath)
		}
	}
}

func compressFile(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	gzipFilePath := filePath + ".gz"
	gzipFile, err := os.Create(gzipFilePath)
	if err != nil {
		return err
	}
	defer gzipFile.Close()

	gzipWriter := gzip.NewWriter(gzipFile)
	defer gzipWriter.Close()

	_, err = io.Copy(gzipWriter, file)
	if err != nil {
		return err
	}

	return nil
}
